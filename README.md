# Lenovo ThinkPad X13s BIOS update - Unix edition

This collection of information is meant to help you updating the BIOS firmware of your aarch64 ThinkPad X13s Gen 1. Maybe the instructions might help updating other (aarch64) computer's firmware too. Who knows.

## Supported runtimes

The basic concept of this script and all required software is supported on mos Linux distributions, as well as FreeBSD, OpenBSD and NetBSD - even though the provided partitioning commands highly differ in BSDs.

## Get your copy of the Windows firmware updater

- Download the Windows arm64 executable from the [ThinkPad X13s Gen 1 support page](https://support.lenovo.com/us/en/downloads/ds556845-bios-update-utility-bootable-cd-for-windows-11-thinkpad-x13s-gen-1-type-21bx-21by).
- In case the link is broken, search for `ThinkPad X13s driver download` and look for a firmware update installer.

## Dependencies

Some investigation shows the installer provided by Lenovo is nothing but an Inno Setup archive. Of course, our holy open source operating system of choise is also capable of extracting it.

Therefore install:

- [`innoextract`](https://pkgs.org/search/?q=innoextract)

```shell
mkdir setup-extract
innoextract -d setup-extract /path/to/downloaded/update/utility.exe
```

## Create a bootable flash drive

Using the partitioning tool of your choise, format a USB drive in GPT and create a single FAT32 formatted 0700 (Microsoft basic data) partition.

Probably, other partition options will work too, but about those it's sure they work at the time of writing these instructions.

```shell
# specify the USB drive to use
export X13S_USB="/dev/sdX"

# run gdisk on your pen drive of choise. Attention: All data on it will get lost
gdisk "${X13S_USB}"
p # check you are really using the drive you want to
o # create new GPT layout
y # confirm wiping disk
n # create new partition
1 # set partition number to 1
  # confirm preset
  # confirm preset
ef00 # the partition's hex code of an EFI system partition
w # write changes to drive

# format the partition in fat32
mkfs.msdos "${X13S_USB}1"
```

## Copy firmware update efistub to flash drive

Now, finally mount the formatted flash drive at a point of choise and copy the required efistub and firmware files on to that mount point.

Assuming we are still in our working directory, the required files are:

- `setup-extract/Rfs/Usb/Bootaa64.efi` -> `$MOUNTPOINT/EFI/Boot/Bootaa64.efi`
- `setup-extract/Rfs/Fw/*` -> `$MOUNTPOINT/Flash/`

```shell
mount --mkdir "${X13S_USB}1" /mnt/x13s-firmware
mkdir -p /mnt/x13s-firmware/{EFI/Boot,Flash}
cd setup-extract/code\$GetExtractPath\$ # yes, the directory name contains "$" ...
cp Rfs/Usb/Bootaa64.efi /mnt/x13s-firmware/EFI/Boot/Bootaa64.efi
cp -r Rfs/Fw/* /mnt/x13s-firmware/Flash/
sync
umount /mnt/x13s-firmware
rmdir /mnt/x13s-firmware
```

## Reboot to USB

Now reboot your ThinkPad X13s Gen 1 and press F12 during system startup, select your pen drive and enjoy your firmware upgrade.

